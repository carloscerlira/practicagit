| **Comando** | **Que hace**                                                                      |
|-------------|-----------------------------------------------------------------------------------|
| git config  | configurar variables como nombre y correo, útil para conectarse a gitlab o github |
| git init    | iniciar un repositorio de git en el directorio actual                             |
| git clone   | clonar un repositorio de git                                                      |
| git add     | agregar los cambios al commit                                                     |
| git rm      | borrar los cambios hechos a cierto archivo en el commit                           |
| git mv      | mover archivos                                                                    |
| git commit  | pasar a la fase de commit todos los cambios realizados                            |
| git push    | enviar los cambios realizados a un repositorio remoto                             |
| git reset   | revertir el commit actual                                                         |