| **Comando** | **Que hace**                  |
|-------------|-------------------------------|
| ls          | mostrar directorios/archivos  |
| cd          | entrar a un directorio        |
| mkdir       | crear un directorio           |
| touch       | crear un archivo vacio        |
| rm          | borrar un archivo             |
| cp          | copiar un archivo             |
| mv          | mover una archivo             |
| vim         | editar un archivo usando vim  |
| nano        | editar un archivo usando nano |
| chmod       | dar permisos                  |
| tar         | comprimir archivos            |